var express = require('express');
var router = express.Router();

var Student = require('../controllers/student')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' })
});

/* GET students list page. */
router.get('/students', function(req, res) {
  // Data retrieve
  Student.list()
    .then(data =>
      res.render('students', { list: data })
    )
    .catch(err => res.render('error', {error: err}))
});

/* GET students registration page. */
router.get('/students/register', function(req, res) {
  res.render('student_form', { title: 'Student Registration', action: '/students', student: '', date: '20/02/2021' })
});

/* POST insert new student in the database. */
router.post('/students', function(req, res) {
  // Insert student in the database
  Student.insert(req.body)
    .then(data => res.redirect('/students'))
    .catch(err => res.render('error', {error: err}))
});

/* GET student personal page. */
router.get('/students/:id', function(req, res) {
  let studentId = req.params.id.toUpperCase();

  // Obtain student
  Student.lookup(studentId)
    .then(student => {
      if (student != null){
        res.render('student', { title: 'Student - ' + studentId, student: student, date: '20/02/2021' })
      }else{
        let err = 'Student ' + studentId + ' not found.'
        res.render('error', {title: err, message: err, error: err})
      }
    })
    .catch(err => res.render('error', {error: err}))
});

/* GET students registration page. */
router.get('/students/edit/:id', function(req, res) {
  let studentId = req.params.id.toUpperCase();

  // Obtain student
  Student.lookup(studentId)
    .then(student => {
      if (student != null){
        res.render('student_form', { title: 'Edit Student - ' + studentId, action: '', student: student, date: '20/02/2021' })
      }else{
        let err = 'Student ' + studentId + ' not found.'
        res.render('error', {title: err, message: err, error: err})
      }
    })
    .catch(err => res.render('error', {error: err}))
});

/* PUT edit student info and insert it in DB. */
router.put('/students/edit/:id', function(req, res) {
  // Update student in the database
  Student.update(req.body)
    .then(data => res.redirect('/students'))
    .catch(err => res.render('error', {error: err}))
});

/* DELETE student from the database. */
router.delete('/students/delete', function(req, res) {
  // Insert student in the database
  Student.delete(req.body.id)
    .then(data => res.redirect('/students'))
    .catch(err => res.render('error', {error: err}))
});

module.exports = router;
