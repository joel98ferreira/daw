// Student controller

var Student = require('../models/student');

// Returns all students
module.exports.list = () => {
    return Student
        .find()
        .sort({nome:1})
        .exec()
}

// Lookup for a Student by its id
module.exports.lookup = id => {
    return Student
        .findOne({numero: id})
        .exec()
}

// Insert a new student
module.exports.insert = student => {
    student.numero = student.numero.toUpperCase()
    var newStudent = new Student(student)
    return newStudent.save()
}

// Update student
module.exports.update = student => {
    return Student
        .updateOne(
            {numero: student.numero},
            {$set: {"nome": student.nome}, $set: {"git": student.git}}
        )
        .exec()
}

// Delete student
module.exports.delete = id => {
    return Student
        .deleteOne({numero: id})
        .exec()
}

