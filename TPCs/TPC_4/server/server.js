var http = require('http');
var fs   = require('fs');


// File paths
const index = './site/index.html';
const page404 = './site/404.html';
const w3css = './site/css/w3.css';
const toccss = './site/css/toc.css';
const notfoundcss = './site/css/404.css';
const arq_elem = './site/docs/arq';
const html = '.html';


/**
 * Create the Node JS server and pass it a function the receive requests 
 * and answer; 
 */
http.createServer(function(req, res) {
    console.log(req.method + " " + req.url + " " + currentDateTime());

    if (req.url.match('^/index.html')){
        // Send the index HTML
        getFile(index, res);
    } else if (req.url.match('^/arqs\/[0-9]+$')){
        // Fetch the arq element and send it to the client
        fetchArqElement(req, res);
    } else if (req.url.match('^/css/w3.css$')){
        // Send the w3 CSS
        getFile(w3css, res);
    } else if (req.url.match('^/css/toc.css$')){
        // Send the toc CSS
        getFile(toccss, res);
    } else if (req.url.match('^/arqs/css/404.css$')){
        // Send the not found CSS
        getFile(notfoundcss, res);
    } else {
        // Send error 404
        res.writeHead(404, {'Content-Type': 'text/html; charset=utf-8'});
        res.write("<h1>O pedido não foi reconhecido pelo servidor.</h1><a href=\"index.html\">Ir para a página inicial.</a>");
        res.end();
    }

}).listen(7777);
console.log("Servidor à escuta na porta 7777...");


/**
 * function to obtain a file given a file path and to write the response
 * header and responde content;
 * @param {*} filePath 
 * @param {*} res
 */
function getFile(filePath, res){
    fs.access(filePath, fs.F_OK, (err) => {
        if (err) { // File doesn't exists
            // Error 404, not found, send the 404 page
            fs.readFile(page404, function(err, data) {
                res.writeHead(404, {'Content-Type': 'text/html; charset=utf-8'});
                res.write(data);
                res.end();
            });
            return;
        }
        
        // File exists
        fs.readFile(filePath, function(err, data) {
            res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
            res.write(data);
            res.end();
        });
    });
}


/**
 * function that given a arq element request and a response, gets the file
 * and writes the response to the client. If the arq element is not found
 * then it sends to the client a 404 default page.
 * @param {*} req 
 * @param {*} res 
 */
function fetchArqElement(req, res){
    var request_split = req.url.split('/');
    var arq_num = request_split[request_split.length-1] == '' ? parseInt(request_split[request_split.length-2]) : parseInt(request_split[request_split.length-1]);

    if (arq_num > 0){
        // Send the HTML
        getFile(arq_elem + arq_num + html, res);
    }
}


/**
 * function to obtain the currente date time;
 */
currentDateTime = function() {
    var d = new Date();
    return d.toISOString().substring(0, 16);
}