<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="2.0">
    
    <xsl:template match="/">
        <xsl:result-document href="index.html">
            <html>
                <head>
                    <title>Arqueossítios do Nordeste Português</title>
                    <link rel="stylesheet" href="css/toc.css"/>
                    <link rel="stylesheet" href="css/w3.css"/>
                </head>
                <body>
                    <h2>Arqueossítios do Nordeste Português</h2>
                    
                    <div id="toc_container">
                        <h3 class="toc_title">Índice de Arqueossítios</h3>
                        <ol class = "toc_list">
                            <xsl:apply-templates select="//ARQELEM" mode="indice">
                                <xsl:sort select="IDENTI"/>
                            </xsl:apply-templates>
                        </ol>
                    </div>
                </body>
            </html>
        </xsl:result-document>
        
        <xsl:apply-templates select="//ARQELEM" mode="content">
            <xsl:sort select="IDENTI" />
        </xsl:apply-templates>
    </xsl:template>

    
    <!-- Templates de índice ................... -->
    
    <xsl:template match="ARQELEM" mode="indice">
        <li>
            <a name="i{generate-id()}"/>
            <a href="arqs/{position()}">
                <xsl:value-of select="IDENTI"/>
            </a>
        </li>
    </xsl:template>
    
    <!-- Templates de conteúdo ................... -->
    
    <xsl:template match="ARQELEM" mode="content">
        <xsl:result-document href="docs/arq{position()}.html">
            <html>
                <head>
                    <title><xsl:value-of select="IDENTI"/></title>
                    <link rel="stylesheet" href="../css/w3.css"/>
                </head>
                <body>
                    
                    <div class="w3-card-4">
                        
                        <header class="w3-container w3-pale-green">
                            <h1><xsl:value-of select="IDENTI"/></h1>
                        </header>
                        
                        <div class="w3-container w3-pale-yellow">
                            <ul class="w3-ul">
                                <li><p><b>Descrição: </b> <xsl:value-of select="DESCRI"/></p></li>
                                
                                <!-- Caso não exista valor associado à cronologia, então não o apresenta -->
                                <xsl:choose>
                                    <xsl:when test="not(empty(CRONO))">
                                        <li><p><b>Cronologia: </b> <xsl:value-of select="CRONO"/></p></li>
                                    </xsl:when>
                                </xsl:choose>
                                
                                <li><p><b>Lugar: </b> <xsl:value-of select="LUGAR"/></p></li>
                                <li><p><b>Freguesia: </b> <xsl:value-of select="FREGUE"/></p></li>
                                <li><p><b>Concelho: </b> <xsl:value-of select="CONCEL"/></p></li>
                                
                                <!-- Caso não exista valor associado ao código administrativo, então não o apresenta -->
                                <xsl:choose>
                                    <xsl:when test="not(empty(CODADM))">
                                        <li><p><b>Código Administrativo: </b> <xsl:value-of select="CODADM"/></p></li>
                                    </xsl:when>
                                </xsl:choose>
                                
                                <!-- Caso não exista valor associado à latitude ou longitude, então não apresenta nenhum deles -->
                                <xsl:choose>
                                    <xsl:when test="not(empty(LATITU)) and not(empty(LONGIT))">
                                        <li><p><b>Latitude: </b> <xsl:value-of select="LATITU"/></p></li>
                                        <li><p><b>Longitude: </b> <xsl:value-of select="LONGIT"/></p></li>
                                    </xsl:when>
                                </xsl:choose>
                                
                                <!-- Caso não exista valor associado à altitude, então não o apresenta -->
                                <xsl:choose>
                                    <xsl:when test="not(empty(ALTITU))">
                                        <li><p><b>Altitude: </b> <xsl:value-of select="ALTITU"/></p></li>
                                    </xsl:when>
                                </xsl:choose>
                                
                                <!-- Caso não exista valor associado ao acesso, então não o apresenta -->
                                <xsl:choose>
                                    <xsl:when test="not(empty(ACESSO))">
                                        <li><p><b>Acesso: </b> <xsl:value-of select="ACESSO"/></p></li>
                                    </xsl:when>
                                </xsl:choose>
                                
                                <!-- Caso não exista valor associado ao quadro, então não o apresenta -->
                                <xsl:choose>
                                    <xsl:when test="not(empty(QUADRO))">
                                        <li><p><b>Quadro: </b> <xsl:value-of select="QUADRO"/></p></li>
                                    </xsl:when>
                                </xsl:choose>
                                
                                <!-- Caso não existam os seguintes valores, então não os apresenta -->
                                
                                <xsl:choose>
                                    <xsl:when test="not(empty(AUTOR))">
                                        <li><p><b>Autor: </b> <xsl:value-of select="AUTOR"/></p></li>
                                    </xsl:when>
                                </xsl:choose>
                                
                                <xsl:choose>
                                    <xsl:when test="not(empty(DEPOSI))">
                                        <li><p><b>Depósito: </b> <xsl:value-of select="DEPOSI"/></p></li>
                                    </xsl:when>
                                </xsl:choose>
                                
                                <xsl:choose>
                                    <xsl:when test="not(empty(DESARQ))">
                                        <div class="w3-panel w3-leftbar w3-sand">
                                            <li><p><b>Descrição arqueológica: </b> <xsl:value-of select="DESARQ"/></p></li>
                                        </div>
                                    </xsl:when>
                                </xsl:choose>
                                
                                <xsl:choose>
                                    <xsl:when test="not(empty(INTERE))">
                                        <li><p><b>Interesse: </b> <xsl:value-of select="INTERE"/></p></li>
                                    </xsl:when>
                                </xsl:choose>
                                
                                
                                <xsl:choose>
                                    <xsl:when test="not(empty(INTERP))">
                                        <li><p><b>Interpretação: </b> <xsl:value-of select="INTERP"/></p></li>
                                    </xsl:when>
                                </xsl:choose>
                                
                                <xsl:choose>
                                    <xsl:when test="not(empty(TRAARQ))">
                                        <li><p><b>Trabalhos arqueológicos: </b> <xsl:value-of select="TRAARQ"/></p></li>
                                    </xsl:when>
                                </xsl:choose>
                            </ul>
                            
                            
                            <xsl:choose>
                                <xsl:when test="not(empty(BIBLIO))">
                                    <div class="w3-panel w3-leftbar">
                                        <p><b>Bibliografia: </b></p>
                                        <ul style="margin-right:2%;">
                                            <xsl:for-each select="BIBLIO">
                                                <li class="simpletext">
                                                    <xsl:apply-templates/>
                                                </li>
                                            </xsl:for-each>
                                        </ul>
                                    </div>
                                </xsl:when>
                            </xsl:choose>
                        </div>
                        
                        <footer class="w3-container w3-pale-blue">
                            <p><b>Data: </b> <xsl:value-of select="DATA"/></p>
                            <address>
                                [<a href="../index.html#i{generate-id()}">Voltar à Página Inicial</a>]
                            </address>
                        </footer>
                        
                    </div>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
    
</xsl:stylesheet>