/**
 * @description Trabalho de Casa Número 5 para a UC de Desenvolvimento
 * de Aplicações WEB do Mestrado Integrado em Engenharia Informática da
 * Universidade do Minho.
 * 
 * @author Joel Ferreira
 * 
 */


var http = require('http')
var fs   = require('fs');
var axios = require('axios')

// File paths
const json_server_host = 'http://localhost'
const json_server_port = 3000;
const http_server_port = 4000;

const index = './public/index.html';
const page404 = './public/404.html';


http.createServer(function(req, res) {
    console.log(req.method + " " + req.url + " " + currentDateTime());
    if (req.method == 'GET'){
        if (req.url == '/'){
            // Send the index HTML
            getFile(index, res);
        }
        // GET a Student by it's ID
        else if (req.url.match('^/alunos/[a-zA-Z0-9]+')){
            axios.get(json_server_host + ':' + json_server_port + req.url)
                .then(  response => alunoResponse(res, response) )
                .catch( error    => errorGET(error, req, res, "aluno") );
        }
        // GET multiple students, query parameter is optional
        else if (req.url.match('^/alunos("?")?')){
            axios.get(json_server_host + ':' + json_server_port + req.url)
                .then(  response => alunosResponse(res, response) )
                .catch( error    => errorGET(error, req, res, "alunos") );
        }
        // GET a course by it's ID
        else if (req.url.match('^/cursos/[a-zA-Z0-9]+')){
            axios.get(json_server_host + ':' + json_server_port + req.url)
                .then(  response => cursoResponse(res, response) )
                .catch( error    => errorGET(error, req, res, "curso") );
        }
        // GET multiple courses, query parameter is optional
        else if (req.url.match('^/cursos("?")?')){
            axios.get(json_server_host + ':' + json_server_port + req.url)
                .then(  response => cursosResponse(res, response) )
                .catch( error    => errorGET(error, req, res, "cursos") );
        }
        // GET an instrument by it's ID
        else if (req.url.match('^/instrumentos/[a-zA-Z0-9]+')){
            axios.get(json_server_host + ':' + json_server_port + req.url)
            .then(  response => instrumentoResponse(res, response) )
            .catch( error    => errorGET(error, req, res, "instrumento") );
        }
        // GET multiple instruments, query parameter is optional
        else if (req.url.match('^/instrumentos("?")?')){
            axios.get(json_server_host + ':' + json_server_port + req.url)
            .then(  response => instrumentosResponse(res, response) )
            .catch( error    => errorGET(error, req, res, "instrumentos") );
        }
        else {
            res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
            res.write('<h1>Pedido não suportado: ' + req.method + ' ' + req.url + '</h1><a href="/">Ir para a página inicial.</a>');
            res.end();
        }
    }else{
        res.writeHead(404, {'Content-Type': 'text/html; charset=utf-8'});
        res.write('<h1>O pedido não foi reconhecido pelo servidor: ' + req.method + " " + req.url + '</h1><a href="/">Ir para a página inicial.</a>');
        res.end();
    }
}).listen(4000);
console.log("Servidor à escuta na porta " + http_server_port + "...");


/**
 * function that given an error verify if it's 404 and return a 404 page or a not recognized page;
 * @param {*} error 
 * @param {*} req 
 * @param {*} res 
 * @param {*} location 
 */
function errorGET(error, req, res, location){
    if (error.response != null && error.response.status == 404){
        getFile(page404, res); 
    }else{
        res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
        res.write('<h1>O pedido não foi reconhecido pelo servidor: ' + req.method + " " + req.url + '</h1><a href="/">Ir para a página inicial.</a>');
        res.end();
    }
    console.log('Erro at "' + location + '": ' + error) 
}


/**
 * function that given a response body with multiple students creates an index of students;
 * @param {*} res 
 * @param {*} resp 
 */
function alunosResponse(res, resp) {
    alunos = resp.data;
    res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
    res.write("<h2>Escola de música: Lista de Alunos</h2>");
    res.write('<ul>');

    alunos.forEach(a => {
        res.write('<li>' + '<a id=' + a.id + ' href="/alunos/' + a.id + '">' + a.id + '</a>' + ' - ' + a.nome + '</li>');
    });

    res.write('</ul>');

    // Print next, previous, first, last, back to home
    if (resp.headers['link'] != null){
        var parsedLink = parseLink(resp.headers['link']);
        console.log(parsedLink)
        if (parsedLink['prev'] == null){
            res.write('<addresss>[<a href="' + parsedLink['first'] + '">Primeira</a>] | [<a href="' + parsedLink['next'] + '">Seguinte</a>] | [<a href="' + parsedLink['last'] + '">Última</a>]</address>');
            res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
            res.end();
        } else if (parsedLink['next'] == null) {
            res.write('<addresss>[<a href="' + parsedLink['first'] + '">Primeira</a>] | [<a href="' + parsedLink['prev'] + '">Anterior</a>] | [<a href="' + parsedLink['last'] + '">Última</a>]</address>');
            res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
            res.end();
        } else{
            res.write('<addresss>[<a href="' + parsedLink['first'] + '">Primeira</a>] | [<a href="' + parsedLink['prev'] + '">Anterior</a>] | [<a href="' + parsedLink['next'] + '">Seguinte</a>] | [<a href="' + parsedLink['last'] + '">Última</a>]</address>');
            res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
            res.end();
        }
    }else{
        res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
        res.end();
    }
}


/**
 * function that writes an response to the client with a student as html;
 * @param {*} res 
 * @param {*} resp 
 */
function alunoResponse(res, resp) {
    aluno = resp.data;
    res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
    res.write("<h1>Aluno " + aluno.id + "</h1>");
    res.write("<h2>Nome: " + aluno.nome + "</h2>");
    res.write("<h2>Data de Nascimento: " + aluno.dataNasc + "</h2>");
    res.write('<h2>Curso: <a href="/cursos/' + aluno.curso + '">' + aluno.curso + '</a></h2>');
    res.write("<h2>Ano Curso: " + aluno.anoCurso + "</h2>");
    res.write("<h2>Instrumento: " + aluno.instrumento + "</h2>");
    res.write('<addresss>[<a href="/alunos#' + aluno.id + '">Voltar</a>]</address>');

    res.end();
}


/**
 * function that given a response body with multiple courses creates an index of courses;
 * @param {*} res 
 * @param {*} resp 
 */
function cursosResponse(res, resp) {
    cursos = resp.data;
    res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'});
    res.write("<h2>Escola de música: Lista de Cursos</h2>");
    res.write('<ul>');

    cursos.forEach(c => {
        res.write('<li>' + '<a id=' + c.id + ' href="/cursos/' + c.id + '">' + c.id + '</a>' + ' - ' + c.designacao + '</li>');
    });
    
    res.write('</ul>');
    
    // Print next, previous, first, last, back to home
    if (resp.headers['link'] != null){
        var parsedLink = parseLink(resp.headers['link']);
        console.log(parsedLink)
        if (parsedLink['prev'] == null){
            res.write('<addresss>[<a href="' + parsedLink['first'] + '">Primeira</a>] | [<a href="' + parsedLink['next'] + '">Seguinte</a>] | [<a href="' + parsedLink['last'] + '">Última</a>]</address>');
            res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
            res.end();
        } else if (parsedLink['next'] == null) {
            res.write('<addresss>[<a href="' + parsedLink['first'] + '">Primeira</a>] | [<a href="' + parsedLink['prev'] + '">Anterior</a>] | [<a href="' + parsedLink['last'] + '">Última</a>]</address>');
            res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
            res.end();
        } else{
            res.write('<addresss>[<a href="' + parsedLink['first'] + '">Primeira</a>] | [<a href="' + parsedLink['prev'] + '">Anterior</a>] | [<a href="' + parsedLink['next'] + '">Seguinte</a>] | [<a href="' + parsedLink['last'] + '">Última</a>]</address>');
            res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
            res.end();
        }
    }else{
        res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
        res.end();
    }
}


/**
 * function that writes an response to the client with a course as html;
 * @param {*} res 
 * @param {*} resp 
 */
function cursoResponse(res, resp) {
    curso = resp.data;
    res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
    res.write("<h1>Curso " + curso.id + "</h1>");
    res.write("<h2>Designação: " + curso.designacao + "</h2>");
    res.write("<h2>Duração: " + curso.duracao + "</h2>");
    res.write('<h2>Instrumento: <a href="/instrumentos/' + curso.instrumento.id + '">' + curso.instrumento.id + '</a> - ' + curso.instrumento['#text'] + '</h2>');
    res.write('<addresss>[<a href="/cursos#' + curso.id + '">Voltar</a>]</address>');
    
    res.end();
}


/**
 * function that given a response body with multiple instruments creates an index of instruments;
 * @param {*} res 
 * @param {*} resp 
 */
function instrumentosResponse(res, resp) {
    instrumentos = resp.data;
    res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'});
    res.write("<h2>Escola de música: Lista de Instrumentos</h2>");
    res.write('<ul>');

    instrumentos.forEach(i => {
        res.write('<li>' + '<a id=' + i.id + ' href="/instrumentos/' + i.id + '">' + i.id + '</a>' + ' - ' + i['#text'] + '</li>');
    });
    
    res.write('</ul>');

    // Print next, previous, first, last, back to home
    if (resp.headers['link'] != null){
        var parsedLink = parseLink(resp.headers['link']);
        if (parsedLink['prev'] == null){
            res.write('<addresss>[<a href="' + parsedLink['first'] + '">Primeira</a>] | [<a href="' + parsedLink['next'] + '">Seguinte</a>] | [<a href="' + parsedLink['last'] + '">Última</a>]</address>');
            res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
            res.end();
        } else if (parsedLink['next'] == null) {
            res.write('<addresss>[<a href="' + parsedLink['first'] + '">Primeira</a>] | [<a href="' + parsedLink['prev'] + '">Anterior</a>] | [<a href="' + parsedLink['last'] + '">Última</a>]</address>');
            res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
            res.end();
        } else{
            res.write('<addresss>[<a href="' + parsedLink['first'] + '">Primeira</a>] | [<a href="' + parsedLink['prev'] + '">Anterior</a>] | [<a href="' + parsedLink['next'] + '">Seguinte</a>] | [<a href="' + parsedLink['last'] + '">Última</a>]</address>');
            res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
            res.end();
        }
    }else{
        res.write('<addresss>[<a href="/">Voltar à Página Inicial</a>]</address>');
        res.end();
    }
}


/**
 * function that writes an response to the client with an instrument as html;
 * @param {*} res 
 * @param {*} resp 
 */
function instrumentoResponse(res, resp) {
    instrumento = resp.data;
    res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
    res.write("<h1>Instrumento " + instrumento.id + "</h1>");
    res.write("<h2>" + instrumento['#text'] + "</h2>");
    res.write('<addresss>[<a href="/instrumentos#' + instrumento.id + '">Voltar</a>]</address>');
    
    res.end();
}


/**
 * function to obtain a file given a file path and to write the response
 * header and responde content;
 * @param {*} filePath 
 * @param {*} res
 */
function getFile(filePath, res){
    fs.access(filePath, fs.F_OK, (err) => {
        if (err) { // File doesn't exists
            // Error 404, not found, send the 404 page
            fs.readFile(page404, function(err, data) {
                res.writeHead(404, {'Content-Type': 'text/html; charset=utf-8'});
                res.write(data);
                res.end();
            });
            return;
        }
        
        // File exists
        fs.readFile(filePath, function(err, data) {
            res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
            res.write(data);
            res.end();
        });
    });
}


/**
 * function that by receiving a link parameter from the request header, parse it's
 * content into a dictionary;
 * @param {*} data 
 */
function parseLink(data) {
    let arrData = data.split("link:")
    data = arrData.length == 2? arrData[1]: data;
    let parsed_data = {}

    arrData = data.split(",")

    for (d of arrData){
        linkInfo = /<([^>]+)>;\s+rel="([^"]+)"/ig.exec(d)

        parsed_data[linkInfo[2]] = linkInfo[1].replace(json_server_host + ':' + json_server_port, '');
    }

    return parsed_data;
}


/**
 * function to obtain the currente date time;
 */
currentDateTime = function() {
    var d = new Date();
    return d.toISOString().substring(0, 16);
}