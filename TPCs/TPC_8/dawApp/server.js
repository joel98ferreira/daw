var express = require('express')
var bodyParser = require('body-parser')
var templates = require('./html-templates')
var jsonfile = require('jsonfile')
var logger = require('morgan')
var fs = require('fs')

var multer = require('multer')
var upload = multer({dest: 'uploads/'})

var app = express()

// set logger
app.use(logger('dev'))

// parse applicatio/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}))

// paser applicatio/json
app.use(bodyParser.json())

app.use(express.static('public'))

app.get('/', function(req, res){
    var d = new Date().toISOString().substr(0, 16)
    var files = jsonfile.readFileSync('./dbFiles.json')
    res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
    res.write(templates.fileList(files, d))
    res.end()
})

app.get('/files/upload', function(req, res){
    var d = new Date().toISOString().substr(0, 16)
    res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
    res.write(templates.fileForm(d))
    res.end()
})

app.get('/files/download/:fname', function(req, res){
    res.download(__dirname + '/public/filestore/' + req.params.fname)
})

app.post('/files', upload.array('myFiles'), function(req, res){
    // req.file is the 'myFile' file
    // req.body will hold the text fields if any

    let i = 0;
    req.files.forEach(file => {
        let oldPath = __dirname + '/' + file.path
        let newPath = __dirname + '/public/filestore/' + file.originalname

        fs.rename(oldPath, newPath, function(err) {
            if (err) throw err
        })

        var filesJSON = jsonfile.readFileSync('./dbFiles.json')
        var d = new Date().toISOString().substr(0, 16)
        filesJSON.push(
            {
                date: d,
                name: file.originalname,
                size: file.size,
                mimetype: file.mimetype,
                desc: req.body.desc[i]
            }
        )

        jsonfile.writeFileSync('./dbFiles.json', filesJSON)
        
        i++;
    });

    res.redirect('/')
})

app.use(function(req, res, next) {
    console.log(JSON.stringify(req.body))
    next()
})

app.listen(7701, () => console.log('Servidor à escuta a porta 7701...'))