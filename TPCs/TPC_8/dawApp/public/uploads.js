function uploadMore(){

    var uploadObj = $(`
    <div id="uploadrow">
        <hr style="border-width: 2px; color: w3-blue;">
        <div class="w3-row w3-margin-bottom">
            <div class="w3-col s3">
                <label class="w3-text-teal">Description</label>
            </div>
            <div class="w3-col s9 w3-border">
                <input class="w3-input w3-border w3-light-grey" type="text" name="desc" required>
            </div>
        </div>
        <div class="w3-row w3-margin-bottom">
            <div class="w3-col s3">
                <label class="w3-text-teal">Select File</label>
            </div>
            <div class="w3-col s9 w3-border">
                <input class="w3-input w3-border w3-light-grey" type="file" name="myFiles" multiple required>
            </div>
        </div>
    </div>     
    `
)
    $("#uploadform").append(uploadObj)
}

function uploadLess(){
    $("#uploadrow:first").remove()
}