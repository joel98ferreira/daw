function geraPagPrincipal(tasks, task_types, edit){
    let pagPrincipal = `
        <html>
            <head>
                <title>Tarefas - DAW 2020</title>
                <meta charset="utf-8"/>
                <link rel="stylesheet" href="w3.css"/>
                <link rel="stylesheet" href="task.css"/>
                <link rel="icon" href="favicon.ico"/>
            </head>
            <body>
    `

    pagPrincipal += geraRegistarTarefas(task_types)

    pagPrincipal += geraListaTarefas(tasks, task_types, edit)

    pagPrincipal += geraTarefasConcluidas(tasks)
    
    pagPrincipal += `            
            </body>
        </html>
    `

    return pagPrincipal
}

exports.geraPagPrincipal = geraPagPrincipal

function geraRegistarTarefas(task_types){
    let pagRegistarTarefas = `
    <div class="w3-row w3-container w3-pale-blue" style="width:100%">
        <div class="w3-col w3-container w3-pale-blue" style="width:50%">
            <h2>Registo de Novas Tarefas</h2>

            <form class="w3-container" action="/tasks" method="POST">
                <div class="form-group">
                    <label class="w3-text-teal"><b>Tarefa:</b></label>
                    <input class="w3-input w3-border w3-light-grey" type="text" name="title" required>
                </div>

                <div class="form-group">
                    <label class="w3-text-teal"><b>Descrição da Tarefa:</b></label>
                    <input class="w3-input w3-border w3-light-grey" type="text" name="description" required>
                </div>

                <div class="form-group">
                    <label class="w3-text-teal"><b>Data Início:</b></label>
                    <input class="w3-input w3-border w3-light-grey" type="date" name="begin_date" required>
                </div>

                <div class="form-group">
                    <label class="w3-text-teal"><b>Data Fim:</b></label>
                    <input class="w3-input w3-border w3-light-grey" type="date" name="due_date" required>
                </div>

                <div class="form-group">
                    <label class="w3-text-teal"><b>Tipo Tarefa:</b></label>
                    <select name="type" required>
                        <option value="">Escolha uma</option>
    `

    task_types.forEach( type => {
        pagRegistarTarefas += `
            <option>${type.description}</option>
    `
    })

    pagRegistarTarefas += `                            
                    </select>
                </div>

                <div class="w3-col w3-container w3-white" style="width:50%">
                    <input class="w3-btn w3-blue-grey" type="submit" value="Registar Tarefa"/>
                </div>

                <div class="w3-col w3-container w3-white" style="width:50%">
                    <input class="w3-btn w3-blue-grey" type="reset" value="Limpar"/> 
                </div>
            </form>
        </div>
    `
    return pagRegistarTarefas
}

function geraListaTarefas(tasks, task_types, edit){
    let pagListaTarefas = `
         <div class="w3-col w3-container w3-pale-green" style="width:50%">
            <h2>Lista de Tarefas</h2>
                        
            <div style="overflow-x:auto;">
                <table class="w3-table w3-striped w3-bordered">
                    <tr class="w3-gray">
                        <th class="w3-text-white">Tarefa</th>
                        <th class="w3-text-white">Descrição</th>
                        <th class="w3-text-white">Data Início</th>
                        <th class="w3-text-white">Data Fim</th>
                        <th class="w3-text-white">Tipo Tarefa</th>
                        <th></th>
                        <th></th>
                    </tr>
    `

    tasks.forEach(task => {
        if (task['completed'].status == 'no'){
            pagListaTarefas += geraLinhaTarefa(task, task_types, edit)
        }
    })

    pagListaTarefas += `  
                </table>
            </div>
            </div>
        </div>
    `

    return pagListaTarefas
}

function geraLinhaTarefa(task, task_types, edit){
    if (edit.option == "yes" && edit.id == task.id){
        let linhaTarefa = `
            <tr>
                <form class="w3-container" action="/tasks/edit" method="POST">
                    <input type="hidden" name="id" value="${task.id}">

                    <td><input class="w3-input w3-border w3-light-grey" type="text" name="title" value="${task.title}"></td>
                    <td><input class="w3-input w3-border w3-light-grey" type="text" name="description" value="${task.description}"></td>
                    <td><input class="w3-input w3-border w3-light-grey" type="date" name="begin_date" value="${task.begin_date}"></td>
                    <td><input class="w3-input w3-border w3-light-grey" type="date" name="due_date" value="${task.due_date}"></td>
                    <td>
                        <select name="type" required>
        `

        task_types.forEach( type => {
            if (type == task.type){
                linhaTarefa += `
                    <option selected>${type.description}</option>
                `
            }else{
                linhaTarefa += `
                    <option>${type.description}</option>
                `                
            }
        })

        linhaTarefa += `
                        </select>
                    </td>
                    <td><input class="w3-btn w3-blue" type="submit" value="Atualizar Tarefa"/></td>
                    <td><a class="w3-btn w3-red" href="/">Cancelar</a></td>
                </form>
            </tr>
        `

        return linhaTarefa

    }else{
        return `
            <tr>
                <form class="w3-container" action="/tasks/edit" method="POST">
                    <input type="hidden" name="id" value="${task.id}">
                    <input type="hidden" name="title" value="${task.title}">
                    <input type="hidden" name="begin_date" value="${task.begin_date}">
                    <input type="hidden" name="due_date" value="${task.due_date}">
                    <input type="hidden" name="type" value="${task.type}">

                    <td>${task.title}</td>
                    <td><input readonly class="w3-input w3-border w3-light-grey" type="text" name="description" value="${task.description}"></td>
                    <td>${task.begin_date}</td>
                    <td>${task.due_date}</td>
                    <td>${task.type}</td>
                    <td><input class="w3-btn w3-green" name="concluir" type="submit" value="Concluir"/></td>
                    <td><input class="w3-btn w3-blue-grey" name="edit" type="submit" value="Editar"></td>
                </form>
            </tr>
        `
    }               
}

function geraTarefasConcluidas(tasks){
    let pagTarefasConcluidas = `
        <div class="w3-row w3-container w3-pale-yellow" style="width:100%">
            <h2>Tarefas Concluídas</h2>

            <table class="w3-table w3-striped w3-bordered">
                <tr class="w3-gray">
                    <th class="w3-text-white">Tarefa</th>
                    <th class="w3-text-white">Descrição</th>
                    <th class="w3-text-white">Data Início</th>
                    <th class="w3-text-white">Data Fim</th>
                    <th class="w3-text-white">Tipo Tarefa</th>
                    <th class="w3-text-white">Data Conclusão</th>
                    <td></th>
                </tr>
    `

    tasks.forEach( task => {
        if (task['completed'].status == "yes"){
            pagTarefasConcluidas += `
                <tr>
                    <form class="w3-container" action="/tasks/delete" method="POST">
                        <input type="hidden" name="id" value="${task.id}">
                        <td>${task.title}</td>
                        <td><input readonly class="w3-input w3-border w3-light-grey" type="text" name="description" value="${task.description}"></td>
                        <td>${task.begin_date}</td>
                        <td>${task.due_date}</td>
                        <td>${task.type}</td>
                        <td>${task['completed'].date}</td>
                        <td><input class="w3-btn w3-red" type="submit" value="Apagar"></td>
                    </form>    
                </tr>
            `
        } 
    })

    pagTarefasConcluidas += `
            </table>
        </div>
    `

    return pagTarefasConcluidas
}


// POST Confirmation HTML Page Template -------------------------------------
function geraPostConfirm(task, d, info){
    return `
    <html>
    <head>
        <title>POST receipt: ${task.title}</title>
        <meta charset="utf-8"/>
        <link rel="icon" href="favicon.ico"/>
        <link rel="stylesheet" href="w3.css"/>
    </head>
    <body>
        <div class="w3-card-4">
            <header class="w3-container w3-blue">
                <h1>Tarefa: ${task.title}, ` + info + `.</h1>
            </header>

            <div class="w3-container">
                <p><a href="/">Voltar para a página inicial."</a></p>
            </div>

            <footer class="w3-container w3-blue">
                <address>Gerado por Tasks Server::DAW2020 em ${d} - [<a href="/">Voltar</a>]</address>
            </footer>
        </div>
    </body>
    </html>
    `
}

exports.geraPostConfirm = geraPostConfirm

// DELETE Confirmation HTML Page Template -------------------------------------
function geraDeleteConfirm(task, d, info){
    return `
    <html>
    <head>
        <title>DELETE receipt</title>
        <meta charset="utf-8"/>
        <link rel="icon" href="favicon.ico"/>
        <link rel="stylesheet" href="w3.css"/>
    </head>
    <body>
        <div class="w3-card-4">
            <header class="w3-container w3-blue">
                <h1>Tarefa ` + info + `.</h1>
            </header>

            <div class="w3-container">
                <p><a href="/">Voltar para a página inicial."</a></p>
            </div>

            <footer class="w3-container w3-blue">
                <address>Gerado por Tasks Server::DAW2020 em ${d} - [<a href="/">Voltar</a>]</address>
            </footer>
        </div>
    </body>
    </html>
    `
}

exports.geraDeleteConfirm = geraDeleteConfirm