var http = require('http')
var axios = require('axios')
var static = require('./static')
var templates = require('./templates')

var {parse} = require('querystring')

// Aux. Functions
// Retrieves student info from request body --------------------------------
function recuperaInfo(request, callback){
    if(request.headers['content-type'] == 'application/x-www-form-urlencoded'){
        let body = ''
        request.on('data', bloco => {
            body += bloco.toString()
        })
        request.on('end', ()=>{
            console.log(body)
            callback(parse(body))
        })
    }
}


// Server setup

var tasksServer = http.createServer(function (req, res) {
    // Logger: que pedido chegou e quando
    var d = new Date().toISOString().substr(0, 16)
    console.log(req.method + " " + req.url + " " + d)

    // Request processing
    // Tests if a static resource is requested
    if(static.recursoEstatico(req)){
        static.sirvoRecursoEstatico(req, res)
    }
    else{
        // Normal request
        switch(req.method){
            case "GET": 
                // GET /tasks --------------------------------------------------------------------
                if((req.url == "/") || (req.url == "/tasks")){
                    console.log('A enviar a página principal...')
                    sendMainPage(res, {'option': 'no', 'id': 'null'})
                }
                else{
                    res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                    res.write("<p>" + req.method + " " + req.url + " não suportado neste serviço.</p>")
                    res.end()
                }
                break
            case "POST":
                if(req.url == '/tasks'){
                    recuperaInfo(req, resultado => {
                        // Add Info about the task
                        addTaskInfo(resultado, null);
                        console.log('POST de uma task:' + JSON.stringify(resultado))
                        // Through indexer
                        axios.post('http://localhost:3000/tasks', resultado)
                            .then(resp => {
                                res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                                res.write(templates.geraPostConfirm(resp.data, d, 'inserida'))
                                res.end()
                            })
                            .catch(erro => {
                                res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                                res.write('<p>Erro no POST: ' + erro + '</p>')
                                res.write('<p><a href="/">Voltar</a></p>')
                                res.end()
                            })
                    })
                } else if(req.url == '/tasks/edit'){
                    recuperaInfo(req, resultado => {
                        // Caso em que se pretende obter a página de edição de uma tarefa 
                        if (resultado.edit != null){
                            console.log('Enviar a página de edição...')
                            sendMainPage(res, {'option': 'yes', 'id': resultado.id})
                        // Caso em que se pretende concluir uma tarefa    
                        }else if (resultado.concluir != null){
                            delete resultado['concluir']
                            addTaskInfo(resultado, {'status': 'yes', 'date': d})
                            console.log('PUT da task concluída:' + JSON.stringify(resultado))

                            axios.put('http://localhost:3000/tasks/' + resultado.id, resultado)
                            .then(resp => {
                                res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                                res.write(templates.geraPostConfirm(resp.data, d, 'concluída'))
                                res.end()
                            })
                            .catch(erro => {
                                res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                                res.write('<p>Erro no PUT: ' + erro + '</p>')
                                res.write('<p><a href="/">Voltar</a></p>')
                                res.end()
                            })
                        // Caso em que é um PUT normal    
                        }else{
                            addTaskInfo(resultado, null)
                            console.log('PUT da task:' + JSON.stringify(resultado))

                            axios.put('http://localhost:3000/tasks/' + resultado.id, resultado)
                            .then(resp => {
                                res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                                res.write(templates.geraPostConfirm(resp.data, d, 'alterada'))
                                res.end()
                            })
                            .catch(erro => {
                                res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                                res.write('<p>Erro no PUT: ' + erro + '</p>')
                                res.write('<p><a href="/">Voltar</a></p>')
                                res.end()
                            })
                        }
                    })
                } else if(req.url == '/tasks/delete'){
                    recuperaInfo(req, resultado => {
                        console.log('DELETE da task concluída:' + JSON.stringify(resultado))
                        axios.delete('http://localhost:3000/tasks/' + resultado.id)
                            .then(resp => {
                                res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                                res.write(templates.geraPostConfirm(resultado.id, d, 'apagada'))
                                res.end()
                            })
                            .catch(erro => {
                                res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                                res.write('<p>Erro no DELETE: ' + erro + '</p>')
                                res.write('<p><a href="/">Voltar</a></p>')
                                res.end()
                            })
                    })
                }
                break
            default: 
                res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                res.write("<p>" + req.method + " não suportado neste serviço.</p>")
                res.end()
        }
    }
})

tasksServer.listen(7779)
console.log('Servidor à escuta na porta 7779...')


function sendMainPage(res, edit){
    
    // Obtain task types
    axios.get("http://localhost:3000/task_types")
        .then(responseTypes => {
            var task_types = responseTypes.data
            // Obtain tasks
            axios.get("http://localhost:3000/tasks?_sort=due_date")
                .then(responseTasks => {
                    var tasks = responseTasks.data

                    res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                    res.write(templates.geraPagPrincipal(tasks, task_types, edit))
                    res.end()
            })
            .catch(erroTasks => {
                console.log(erroTasks)
                res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
                res.write("<p>Não foi possível obter as tarefas...")
                res.end()
            })
        })
        .catch(erroTypes => {
            console.log(erroTypes)
            res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'})
            res.write("<p>Não foi possível obter os tipos de tarefas tarefas...")
            res.end()
        })
}


function addTaskInfo(task, info){
    if (info != null){
        task['completed'] = info
    }else{
        task['completed'] = {'status': 'no', 'date': 'null'} 
    }
}